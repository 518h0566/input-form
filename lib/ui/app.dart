import 'package:flutter/material.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Input",
      home: Scaffold(
        appBar: AppBar(title: Text('Input form - v0.0.3')),
        body: InputScreen(),
      ),
    );
  }

}

class InputScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return InputState();
  }

}

class InputState extends State<StatefulWidget> {
  final formKey = GlobalKey<FormState>();
  late String emailAddress;
  late String SurnameAndMiddlename;
  late String NameText;
  late String AddressText;
  late String DayofBirth;


  @override
  Widget build(BuildContext context) {
    return Container(
      child: Form(
        key: formKey,
        child: Column(
          children: [
            fieldEmailAddress(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            fieldSurnameAndMiddlename(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            fieldName(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            fieldDayofBirth(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            fieldAddress(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            InputButton()
          ],
        ),
      )

    );
  }

  Widget fieldEmailAddress() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        icon: Icon(Icons.person),
        labelText: 'Email address'
      ),
      validator: (value) {
        if (value!.length < 1 || !value.contains('@')) {
          return 'Pls input valid email.';
        }
        return null;
      },
      onSaved: (value) {
        emailAddress = value as String;
      },
    );
  }

  Widget fieldSurnameAndMiddlename(){
    return TextFormField(
      decoration: InputDecoration(
          icon: Icon(Icons.person),
          labelText: 'Surname & Middlename'
      ),
      validator: (value) {
        if (value!.length < 1 ) {
          return 'Pls input Surname & Middlename.';
        }
        return null;
      },
      onSaved: (value) {
        SurnameAndMiddlename = value as String;
      },
    );
  }

  Widget fieldName(){
    return TextFormField(
      decoration: InputDecoration(
          icon: Icon(Icons.person),
          labelText: 'Name'
      ),
      validator: (value) {
        if (value!.length < 1 ) {
          return 'Pls input your Name.';
        }
        return null;
      },
      onSaved: (value) {
        NameText = value as String;
      },
    );
  }

  Widget fieldDayofBirth(){
    return TextFormField(
      decoration: InputDecoration(
          icon: Icon(Icons.person),
          labelText: 'Day of Birth'
      ),
      validator: (value) {
        if (value!.length < 1 ) {
          return 'Pls input your Birth day.';
        }
        return null;
      },
      onSaved: (value) {
        DayofBirth = value as String;
      },
    );
  }

  Widget fieldAddress(){
    return TextFormField(
      decoration: InputDecoration(
          icon: Icon(Icons.person),
          labelText: 'Address'
      ),
      validator: (value) {
        if (value!.length < 1 ) {
          return 'Pls input your Address.';
        }
        return null;
      },
      onSaved: (value) {
        AddressText = value as String;
      },
    );
  }

  Widget InputButton() {
    return ElevatedButton(
        onPressed: () {
          if (formKey.currentState!.validate()) {
            // Call API Authentication from Backend
            formKey.currentState!.save();
            print('$emailAddress, $SurnameAndMiddlename, $NameText, $AddressText');
          }
        },
        child: Text('Input')
    );
  }
}